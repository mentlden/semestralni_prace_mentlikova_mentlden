import turtle

sirka,vyska = 1440, 900             #nastaveno na rozměry obrazovky

def Sierpinsky_triangle(dims = 6):
    fraktal = turtle.Turtle()
    turtle.Screen().bgcolor("black")
    turtle.Screen().setup(sirka, vyska)
    fraktal.penup()
    fraktal.goto(-250,-200)
    fraktal.pendown()
    fraktal.speed(0)
    fraktal.hideturtle()
    fraktal.color("Chartreuse")

    axiom = "A"
    chr_1, rule_1 = "A", "A-B+A+B-A"
    chr_2, rule_2 = "B", "BB"
    step, angle = 8, 120

    def apply_rules(axiom):
        return "".join([rule_1 if chr == chr_1 else 
                        rule_2 if chr == chr_2 else chr for chr in axiom])

    def get_results(dims,axiom):
        for dim in range(dims):
            axiom = apply_rules(axiom)
        return axiom

    turtle.delay(0)             
    turtle.hideturtle()
    turtle.penup()
    turtle.goto(-680, 300)
    turtle.pendown()
    turtle.pencolor("Chartreuse")
    turtle.write(f'Sierpiński triangle', font = ("Arial", 60, "bold"))

    axiom = get_results(dims,axiom)
    for chr in axiom:
        if chr == chr_1 or chr == chr_2:
            fraktal.forward(step)
        elif chr == "+":
            fraktal.right(angle)
        elif chr == "-":
            fraktal.left(angle)

    turtle.Screen().exitonclick()

def Dragon_Curve(dims = 12):
    turtle.Screen().setup(sirka, vyska)
    turtle.Screen().bgcolor("black")
    fraktal = turtle.Turtle()
    fraktal.color("black")
    fraktal.goto(250,0)
    fraktal.speed(500)
    fraktal.hideturtle()
    fraktal.color("DeepPink")

    axiom = "XY"
    chr_1, rule_1 = "X", "X+YA+"
    chr_2, rule_2 = "Y", "-AX-Y"
    step = 4
    angle = 90

    def apply_rules(axiom):
        return "".join([rule_1 if chr == chr_1 else 
                        rule_2 if chr == chr_2 else chr for chr in axiom])

    def get_results(dims,axiom):
        for dim in range(dims):
            axiom = apply_rules(axiom)
        return axiom

    turtle.delay(0)
    turtle.hideturtle()
    turtle.pencolor("black")
    turtle.goto(-680, 300)
    turtle.pencolor("DeepPink")
    turtle.write(f'Dragon curve', font = ("Arial", 60, "bold"))

    axiom = get_results(dims,axiom)
    for chr in axiom:
        if chr == chr_1 or chr == chr_2:
            fraktal.forward(step)
        elif chr == "+":
            fraktal.right(angle)
        elif chr == "-":
            fraktal.left(angle)

    turtle.Screen().exitonclick()

def Koch_Snowflake(dims = 5):
    turtle.Screen().setup(sirka, vyska)
    turtle.Screen().bgcolor("black")

    fraktal = turtle.Turtle()
    fraktal.penup()
    fraktal.goto(-250,150)
    fraktal.pendown()
    fraktal.speed(0)
    fraktal.hideturtle()
    fraktal.color("Cyan")

    axiom = "A++A++A"
    chr_1, rule_1 = "A", "A-A++A-A"
    step, angle = 2, 60

    def apply_rules(axiom):
        return "".join([rule_1 if chr == chr_1 else 
                        chr for chr in axiom])

    def get_results(dims,axiom):
        for dim in range(dims):
            axiom = apply_rules(axiom)
        return axiom

    turtle.delay(0)
    turtle.hideturtle()
    turtle.penup()
    turtle.goto(-680, 300)
    turtle.pendown()
    turtle.pencolor("Cyan")
    turtle.write(f'Koch Snowflake', font = ("Arial", 60, "bold"))

    fraktal.setheading(0)

    axiom = get_results(dims,axiom)
    for chr in axiom:
        if chr == chr_1:
            fraktal.forward(step)
        elif chr == "+":
            fraktal.right(angle)
        elif chr == "-":
            fraktal.left(angle)

    turtle.Screen().exitonclick()

def Branch_Fractal(dims = 7):
    turtle.Screen().setup(sirka, vyska)
    turtle.Screen().bgcolor("black")

    fraktal = turtle.Turtle()
    fraktal.penup()
    fraktal.goto(50,-360)
    fraktal.pendown()
    fraktal.speed(0)
    fraktal.hideturtle()
    fraktal.color("orange")

    axiom = "XY"
    chr_1, rule_1 = "A", "AA"
    chr_2, rule_2 = "X", "A[+X]A[-X]+X"
    step, angle= 2.5, 30
    stack = []

    def apply_rules(axiom):
        return "".join([rule_1 if chr == chr_1 else 
                        rule_2 if chr == chr_2 else chr for chr in axiom])

    def get_results(dims,axiom):
        for dim in range(dims):
            axiom = apply_rules(axiom)
        return axiom

    turtle.delay(0)
    turtle.hideturtle()
    turtle.pencolor("black")
    turtle.goto(-680, 300)
    turtle.pencolor("orange")
    turtle.write(f'Branching fractal', font = ("Arial", 60, "bold"))

    axiom = get_results(dims,axiom)
    fraktal.left(90)
    for chr in axiom:
        if chr == chr_1:
            fraktal.forward(step)
        elif chr == "+":
            fraktal.right(angle)
        elif chr == "-":
            fraktal.left(angle)
        elif chr == "[":
            angle_, pos_ = fraktal.heading(), fraktal.pos()
            stack.append((angle_, pos_))
        elif chr == "]":
            angle_, pos_ = stack.pop()
            fraktal.setheading(angle_)
            fraktal.penup()
            fraktal.goto(pos_)
            fraktal.pendown()

    turtle.Screen().exitonclick()

print("✮ GENERÁTOR FRAKTÁLŮ ✮".center(80))
print("A - Sierpińského trojúhelník ")
print("B - Dračí křivka")
print("C - Kochova vločka")
print("D - Větvený fraktál")
choice = (str(input("Prosím zvolte jednu z možností A-D:")))
print(choice)

if choice == "A" or choice == "a":
    Sierpinsky_triangle()
elif choice == "B" or choice == "b":
    Dragon_Curve()
elif choice == "C" or choice == "c":
    Koch_Snowflake()
elif choice == "D" or choice == "d":
    Branch_Fractal()
else:
    print("Neplatný vstup")


